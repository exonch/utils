package models

import (
	"fmt"
	"time"

	"bitbucket.org/exonch/utils/connpool"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type AMQPSource struct {
	Config   *amqp.Config
	Address  string
	ChanName string

	name    string
	type_   string
	maxconn int

	connPool *connpool.Pool
	chanPool *connpool.Pool
}

func (s *AMQPSource) allocCon() interface{} {
	con, err := amqp.DialConfig(s.Address, *s.Config)
	if err != nil {
		logrus.Errorf("AMQPSource.allocCon: amqp.DialConfig failed: %v", err)
		return nil
	}
	return con
}

func (s *AMQPSource) destroyCon(res interface{}) {
	if res == nil {
		return
	}
	con := res.(*amqp.Connection)
	err := con.Close()
	if err != nil {
		logrus.Errorf("AMQPSource.destroyCon: %v", err)
	}
}

func (s *AMQPSource) allocChan() interface{} {
	var con *amqp.Connection
	var chn *amqp.Channel
	for {
		con_, err := s.connPool.Get()
		if err != nil || con_ == nil {
			logrus.Errorf("AMQPSource.GetConn error: s.connPool.Get failed: %v (%v)", err, con_)
			return nil
		}
		con = con_.(*amqp.Connection)
		if con == nil {
			logrus.Errorf("AMQPSource.GetConn error: s.connPool.Get failed: %v (%v)", err, con_)
			return nil
		}

		chn, err = con.Channel()
		if err != nil {
			amqpErr, ok := err.(*amqp.Error)
			if ok && amqpErr == amqp.ErrClosed {
				logrus.WithFields(logrus.Fields{
					"address":   s.Address,
					"chan_name": s.ChanName,
					"name":      s.name,
					"type":      s.type_,
				}).Warnf("AMQP connection was not open, retrying")
				s.connPool.Put(nil)

				continue
			}

			logrus.Errorf("AMQPSource.allocChan: con.Channel: %[1]T %#[1]v", err)
			return nil
		}

		break
	}
	s.connPool.Put(con)
	return chn
}

func (s *AMQPSource) destroyChan(res interface{}) {
	if res == nil {
		logrus.Errorf("don't do this")
		return
	}
	chn, ok := res.(*amqp.Channel)
	if !ok {
		logrus.Errorf("wtf is this? %T %#v", res, res)
		return
	}
	if err := chn.Close(); err != nil {
		logrus.Errorf("AMQPSource.destroyChan: %v", err)
	}
}

func NewAMQPSource(name, type_ string, maxconn int, addrstr string, cfg *amqp.Config, chanName string) *AMQPSource {
	var s = new(AMQPSource)
	s.Address = addrstr
	s.Config = cfg
	s.name = name
	s.type_ = type_
	s.maxconn = maxconn
	s.connPool = connpool.NewPool(s.allocCon, s.destroyCon, maxconn, time.Second*30)
	s.chanPool = connpool.NewPool(s.allocChan, s.destroyChan, 10, time.Second*30)
	s.ChanName = chanName
	return s
}

func (s *AMQPSource) Name() string {
	return s.name
}

func (s *AMQPSource) Type() string {
	return s.type_
}

func (s *AMQPSource) MaxConnections() int {
	return s.maxconn
}

func (s *AMQPSource) AddressString() string {
	return s.Address
}

func (s *AMQPSource) User() string {
	return "TODO"
}

func (s *AMQPSource) Password() string {
	return "FIXME"
}

func (s *AMQPSource) GetConn() (interface{}, error) {
	_ch, _ := s.chanPool.Get()
	if _ch == nil {
		return nil, fmt.Errorf("failed to get amqp channel")
	}
	ch := _ch.(*amqp.Channel)
	if ch == nil {
		return nil, fmt.Errorf("failed to get amqp channel")
	}

	return ch, nil
}

func (s *AMQPSource) PutConn(chn_ interface{}) (err error) {
	s.chanPool.Put(chn_)
	return
}
