// Package models содержит используемые в схеме модели.
package models

import (
	"errors"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"time"

	"bitbucket.org/exonch/utils/connpool"
	"github.com/sirupsen/logrus"
)

type Source interface {
	Name() string
	Type() string
	MaxConnections() int
	AddressString() string
	User() string
	Password() string
	GetConn() (interface{}, error)
	PutConn(interface{}) error
}

type Sources struct {
	List map[string]Source
}

// JSON RPC Source.
type RPCSource struct {
	name     string
	type_    string
	address  string
	user     string
	password string

	connPool *connpool.Pool
}

// Add добавляет источник в список источников.
func (ss *Sources) Add(s Source) error {
	if ss.List == nil {
		ss.List = make(map[string]Source)
	}

	if err := checkSourceFields(s); err == nil {
		ss.List[s.Name()] = s
		return nil
	} else {
		return err
	}
}

// Get возвращает источник по назаванию из списка источников.
func (ss *Sources) Get(name string) (Source, error) {
	if s, ok := ss.List[name]; ok {
		return s, nil
	}
	return nil, errors.New("Unable source")
}

func (s *RPCSource) alloc() interface{} {
	tcpaddr, err := net.ResolveTCPAddr("tcp", s.address)
	if err != nil {
		logrus.Errorf("error resolving %s: %v", s.address, err)
		return nil
	}
	conn, err := net.DialTCP("tcp", nil, tcpaddr)
	if err != nil {
		logrus.Errorf("dial %s error: %v", tcpaddr.String(), err)
		return nil
	}
	cli := jsonrpc.NewClient(conn)
	return cli
}

func (s *RPCSource) destroy(res interface{}) {
	cli := res.(*rpc.Client)
	cli.Close()
}

func NewRPCSource(name, type_ string, maxconn int, address, user, password string) *RPCSource {
	var s *RPCSource
	s = &RPCSource{
		name:     name,
		type_:    type_,
		address:  address,
		user:     user,
		password: password,
	}
	s.connPool = connpool.NewPool(s.alloc, s.destroy, maxconn, time.Second*4)
	return s
}

func (s *RPCSource) Name() string {
	return s.name
}

func (s *RPCSource) Type() string {
	return s.type_
}

func (s *RPCSource) MaxConnections() int {
	return s.connPool.Max
}

func (s *RPCSource) AddressString() string {
	return s.address
}

func (s *RPCSource) User() string {
	return s.user
}

func (s *RPCSource) Password() string {
	return s.password
}

// returns net.Conn on success, otherwise error
func (s *RPCSource) GetConn() (interface{}, error) {
	rpcCli, err := s.connPool.Get()
	if err != nil {
		return nil, err
	}
	return rpcCli, nil
}

func (s *RPCSource) PutConn(rpcClient interface{}) error {
	var rpcCli interface{}
	rpcCli = rpcClient
	err := s.connPool.Put(rpcCli)
	return err
}

//checkSourceFields checks that all Source's fields are populated.
//TODO: make append error
func checkSourceFields(s Source) error {
	if s.Name() == "" {
		return errors.New("Name required")
	}
	if s.Type() == "" {
		return errors.New("Type required")
	}
	if s.MaxConnections() <= 0 {
		return errors.New("MaxConnection required")
	}
	if s.AddressString() == "" {
		return errors.New("Address required")
	}

	return nil
}
