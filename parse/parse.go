package parse

//Parser allows to take (nested)field from interface{}.
//Char '|' is used as separator. E.g.:
//String("metadata|user") return value of field "user" nested in map "metadata"

import (
	"strings"
)

var sep = "|"

func SetSeparator(_sep string) {
	sep = _sep
}

func Separator() string {
	return sep
}

func String(i interface{}, fields string) (string, bool) {
	fArray := strings.Split(fields, sep)
	var ok bool
	var mp map[string]interface{}
	for _, field := range fArray {
		mp, ok = i.(map[string]interface{})
		if !ok {
			return "", false
		}
		i = mp[field]
	}
	res, ok := i.(string)
	return res, ok
}

func Interface(i interface{}, fields string, action ...interface{}) (interface{}, bool) {
	fArray := strings.Split(fields, sep)
	if len(fArray) == 0 {
		return nil, false
	}
	lastField := fArray[len(fArray)-1]
	actionLen := len(action)
	var ok bool
	var isMap, isSlice bool
	var mp map[string]interface{}
	var stopIter int
	for iter, field := range fArray {
		var mpTmp map[string]interface{}
		var slTmp []interface{}
		mpTmp, isMap = i.(map[string]interface{})
		slTmp, isSlice = i.([]interface{})
		if !isMap && !isSlice {
			break
		}
		if isSlice {
			ret := []interface{}{}
			for i := range slTmp {
				r, ok := Interface(slTmp[i], strings.Join(fArray[iter:], sep), action...)
				if !ok {
					return nil, false
				}
				ret = append(ret, r)
			}
			return ret, true
		}
		stopIter = iter
		mp = mpTmp
		i = mp[field]
	}
	if actionLen == 0 {// get
		if stopIter != len(fArray) - 1 {
			return nil, false
		}
		return i, true
	}
	act, ok := action[0].(string)
	if !ok {
		return i, false
	}
	switch act {
	case "delete":
		if stopIter != len(fArray) - 1 {
			return nil, false
		}
		delete(mp, lastField)
	case "upsert":
		if actionLen != 2 {
			return nil, false
		}
		for iter := stopIter; iter < len(fArray) - 1; iter++ {
			field := fArray[iter]
			newMp := make(map[string]interface{})
			mp[field] = newMp
			mp = newMp
		}
		mp[lastField] = action[1]
	}
	return nil, true
}

func InterfaceArray(i interface{}, fields string) ([]interface{}, bool) {
	_interface, ok := Interface(i, fields)
	if !ok {
		return nil, false
	}
	_interfaceArr, ok := _interface.([]interface{})
	return _interfaceArr, ok
}

func Map(i interface{}, fields string) (map[string]interface{}, bool) {
	_interface, ok := Interface(i, fields)
	if !ok {
		return nil, false
	}
	_map, ok := _interface.(map[string]interface{})
	return _map, ok
}
