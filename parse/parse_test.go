package parse

import (
	"encoding/json"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestString(t *testing.T) {
	sJson := `{
	  "kind": "Namespace",
	  "apiVersion": "v1",
	  "metadata": {
	    "name": "AC-DC",
	    "annotations": {
	      "net.beta.kubernetes.io/network-policy": "{\"ingress\": {\"isolation\": \"DefaultDeny\"}}"
	    }
	  }
	}`
	var i interface{}
	json.Unmarshal([]byte(sJson), &i)
	Convey("call parse.String", t, func() {
		str, ok := String(i, "metadata|name")
		So(ok, ShouldBeTrue)
		So(str, ShouldEqual, "AC-DC")
	})
}

func TestInterface_Delete(t *testing.T) {
	//arrange
	sJson := `{
	  "kind": "Namespace",
	  "apiVersion": "v1",
	  "metadata": {
	    "name": "AC-DC",
	    "annotations": {
	      "net.beta.kubernetes.io/network-policy": "{\"ingress\": {\"isolation\": \"DefaultDeny\"}}"
	    }
	  }
	}`
	deletePath := "metadata|annotations"
	expJson := `{
	  "kind": "Namespace",
	  "apiVersion": "v1",
	  "metadata": {
	    "name": "AC-DC"
	  }
	}`
	var i1, i2 interface{}
	json.Unmarshal([]byte(sJson), &i1)
	json.Unmarshal([]byte(expJson), &i2)
	Convey("call parse.Interface action=delete", t, func() {
		//act
		_, ok := Interface(i1, deletePath, "delete")
		//assert
		So(ok, ShouldEqual, true)
		So(ok, ShouldBeTrue)
		So(i1, ShouldResemble, i2)
	})
}

func TestInterface_Upsert(t *testing.T) {
	//arrange
	sJson := `{
	  "kind": "Namespace",
	  "apiVersion": "v1"
	}`
	upsertPath := "metadata|name"
	upsertObject := "AC-DC"
	expJson := `{
	  "kind": "Namespace",
	  "apiVersion": "v1",
	  "metadata": {
	    "name": "AC-DC"
	  }
	}`
	var i1, i2 interface{}
	json.Unmarshal([]byte(sJson), &i1)
	json.Unmarshal([]byte(expJson), &i2)
	Convey("call parse.Interface action=delete", t, func() {
		//act
		_, ok := Interface(i1, upsertPath, "upsert", upsertObject)
		//assert
		So(ok, ShouldEqual, true)
		So(ok, ShouldBeTrue)
		So(i1, ShouldResemble, i2)
	})

}

func TestInterfaceArray(t *testing.T) {
	sJson := `{
	  "array": [1,2,3,4,5,6,7]
	}`
	var i interface{}
	json.Unmarshal([]byte(sJson), &i)
	Convey("call parse.InterfaceArray", t, func() {
		array, ok := InterfaceArray(i, "array")
		So(ok, ShouldBeTrue)
		So(array[3].(float64), ShouldEqual, 4)
	})
}

func TestMap(t *testing.T) {
	sJson := `{
	  "map": {
	  	"ray": "charles"
	  }
	}`
	var i interface{}
	json.Unmarshal([]byte(sJson), &i)
	Convey("call parse.Map", t, func() {
		_map, ok := Map(i, "map")
		So(ok, ShouldBeTrue)
		So(_map["ray"].(string), ShouldEqual, "charles")
	})
}