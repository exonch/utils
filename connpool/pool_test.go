package connpool

import (
	"testing"

	"time"
	"sync"
)

func TestProbabilistic1(t *testing.T) {
	poolsize := 10
	v := 0
	LogMutex := sync.Mutex{}
	Log := func(f string, args ...interface{}) {
		LogMutex.Lock()
		t.Logf(f, args...)
		LogMutex.Unlock()
	}
	stdpool := &sync.Pool{
		New: func() interface{} {
			v += 1
			t.Logf("%s new v, %d", time.Now().Format("15:04:05.000000000"), v)
			return v
		},
	}
	allocf := func() (r interface{}) {
		r = stdpool.Get()
		Log("%s create", time.Now().Format("15:04:05.000000000"))
		return
	}
	destroyf := func(r interface{}) {
		stdpool.Put(r)
		Log("%s destroy", time.Now().Format("15:04:05.000000000"))
	}
	p := NewPool(allocf, destroyf, poolsize, time.Millisecond)

	wg := sync.WaitGroup{}
	for i := 0; i < 600000; i++ {
		wg.Add(1)
		go func() {
			tmp, _ := p.Get()
			//t.Logf("GOT <%T> %v from our pool (err=%v)", tmp, tmp, err)
			go func() {
				p.Put(tmp)
				//t.Logf("PUT <%T> %v to our pool (err=%v)", tmp, tmp, err1)
				wg.Done()
			}()
		}()
	}
	wg.Wait()
	Log("v=%d\n", v)
	if v != poolsize {
		t.Fatalf("v: expected %d, got %d", poolsize, v)
	}
}
