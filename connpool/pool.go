package connpool

import (
	"container/list"
	"sync"
	"time"
)

type AllocFunc func() interface{}

type DestroyFunc func(interface{})

// Resource pool. Use the NewPool constructor.
// Do not copy this object.
type Pool struct {
	Max           int
	CollectPeriod time.Duration
	KeepCount     int

	avail        *list.List // of connContainer
	count        int
	reintroduced chan interface{} // of resources
	allocf       AllocFunc
	destroyf     DestroyFunc
	stopmanage   chan struct{}

	managed bool
	lock    sync.Mutex
	inited  bool
}

// Creates new Pool, allocf is the function that creates a new
// instance of the resource, destroyf destroys one.
// maxnum specifies the maximum amount of live instances of
// the resource that are allowed to be created. collectperiod
// specifies the time, after which one unused instance is destroyed.
func NewPool(allocf AllocFunc, destroyf DestroyFunc, maxnum int, collectperiod time.Duration) *Pool {
	var p = &Pool{
		Max:           maxnum,
		CollectPeriod: collectperiod,
		KeepCount:     1,

		avail:        list.New(),
		reintroduced: make(chan interface{}, maxnum),
		allocf:       allocf,
		destroyf:     destroyf,
		inited:       true,
	}
	return p
}

// TODO: Pool.init()

// blocks until a connection becomes available
func (p *Pool) Get() (interface{}, error) {
	var res interface{}

	for done := false; done == false; {
		select {
		case res = <-p.reintroduced:
			if res != nil {
				done = true
			} else {
				p.count--
			}
		default:
			p.lock.Lock()
			if !p.managed {
				p.managed = true
				go p.manage()
			}
			if p.count < p.Max {
				res = p.allocf()
				p.count++
				done = true
			} else {
				res = <-p.reintroduced
				if res != nil {
					done = true
				} else {
					p.count--
				}
			}
			p.lock.Unlock()
		}
	}

	return res, nil
}

// TODO: perhaps it makes sense to have Pool.GetDeadline(t time.Time)

// Put reintroduces the specified resource into the pool.
// If res is nil, it assumes that resource died while in possession of
// the client and resource count is decremented by 1.
func (p *Pool) Put(res interface{}) error {
	var err error
	p.reintroduced <- res
	return err
}

// Called as a goroutine. Every p.CollectPeriod seconds
// destroys an unused resource.
func (p *Pool) manage() {
outer:
	for {
		select {
		case <-p.stopmanage:
			break outer
		case <-time.After(p.CollectPeriod):
		}

		p.lock.Lock()
		select {
		case victim := <-p.reintroduced:
			if p.count > p.KeepCount {
				p.count--
				p.destroyf(victim)
			} else {
				p.reintroduced <- victim
			}
		default:
		}
		p.lock.Unlock()
	}
	p.managed = false
}
