package logrus_filehook

import (
	"os"
	"github.com/sirupsen/logrus"
	"fmt"
)

type FileHook struct {
	file *os.File
}

var jFormatter = logrus.JSONFormatter{}

func NewFileHook(file *os.File) *FileHook {
	return &FileHook{file:file}
}

func (fh *FileHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (fh *FileHook) Fire(entry *logrus.Entry) error {
	bMsg, err := jFormatter.Format(entry)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to format entry, %v", err)
		return err
	}
	_, err = fh.file.Write(bMsg)
	return err
}