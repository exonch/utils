package rules

import (
	"fmt"

	"bitbucket.org/exonch/utils/parse"
)

type Method string

const (
	Delete  Method = "delete"
	Upsert  Method = "upsert"
)

var separator = "|"
func SetSeparator(sep string) {
	separator = sep
	parse.SetSeparator(sep)
}

type RuleSet struct{
	Auto      []Rule
	Manual    map[string]Rule
	Separator string
}

func New() *RuleSet {
	rs := RuleSet{
		Manual: make(map[string]Rule),
		Separator: separator,
	}
	return &rs
}

func (rs *RuleSet) AddAutoRule(m Method, path string, value ...interface{}) {
	if len(value) == 0 {
		rs.Auto = append(rs.Auto, newRule(m, path, nil))
		return
	}
	rs.Auto = append(rs.Auto, newRule(m, path, value[0]))
}

//change interface data
func (rs *RuleSet) UseAutoRules(data interface{}) error {
	parse.SetSeparator(rs.Separator)
	for i, rule := range rs.Auto {
		if !useAutoRule(data, rule) {
			return fmt.Errorf("rule %d failed", i)
		}
	}
	return nil
}

func useAutoRule(data interface{}, rule Rule) (ok bool) {
	switch rule.Method {
	case Upsert:
		_, ok = parse.Interface(data, rule.Path, "upsert", rule.Value)
	case Delete:
		_, ok = parse.Interface(data, rule.Path, "delete")
	default:
		ok = false
	}
	return
}

func (rs *RuleSet) AddManualRule(name string, m Method, path string, value interface{}) {
	rs.Manual[name] = newRule(m, path, value)
}

func (rs *RuleSet) GetManualRule(name string) Rule {
	return rs.Manual[name]
}

type Rule struct {
	Method Method
	Path   string
	Value  interface{}
}

func newRule(m Method, path string, value interface{}) Rule {
	return Rule{Method:m, Path: path, Value: value}
}
