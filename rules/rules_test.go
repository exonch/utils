package rules

import (
	"testing"
	"encoding/json"
	. "github.com/smartystreets/goconvey/convey"
	"reflect"
)

var testJson =
	`{"foo":{"foo2": "bar2"},
	"bar":"empty"}`

var testExpectedJson =
	`{"foo":{"foo3":"bar3"},
	"bar":{"foo3":"bar3"}}`

func fatalError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func TestExample(t *testing.T) {
	//arrange
	var test, expected interface{}
	fatalError(json.Unmarshal([]byte(testJson), &test))
	fatalError(json.Unmarshal([]byte(testExpectedJson), &expected))
	rules := New()
	rules.AddAutoRule(Delete, "foo|foo2")
	rules.AddAutoRule(Upsert, "foo|foo3", "bar3")
	object := make(map[string]interface{})
	object["foo3"] = "bar3"
	rules.AddAutoRule(Upsert, "bar", object)
	Convey("Call UseAutoRules", t, func () {
		//act
		rules.UseAutoRules(test)
		//assert
		So(reflect.DeepEqual(test, expected), ShouldBeTrue)
	})

}
