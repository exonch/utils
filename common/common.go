package common 

import "regexp"

func Substitute(src []byte, find, replace string) []byte {
	reg := regexp.MustCompile(find)
	return reg.ReplaceAll(src, []byte(replace))
}
