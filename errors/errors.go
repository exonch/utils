package errors

import "fmt"

type Status string
type Reason string

const (
	//Status list
	StatusInternalErr   Status = "Internal error"
	StatusNotFound      Status = "Not found"
	StatusUnauthorized  Status = "Unauthorized"
	StatusBadRequest    Status = "Bad request"
	StatusAlreadyExists Status = "Object already exists"
	//Reason list
	KubeReadRespErr    Reason = "Failed to read Kubernetes response body"
	KubeNotResponse    Reason = "Kubernetes server is not response"
	KubeConnectFailed  Reason = "Failed to connect to Kubernetes"
	KubeNotFound       Reason = "Kunernetes return 404 StatusNotFound"
	KubeNot2xxStatus   Reason = "Kubernetes return not 2xx Status"
	KubeUnexpectedResp Reason = "Kubernetes return unexpected response"
	KubeAlreadyExists  Reason = "Object already exists"
	KubeInternalErr    Reason = "Kubernetes return internal error"
	ChMngInternalErr   Reason = "Ch-manager internal error"
	AbortWithErr       Reason = "AbortWithError"
	ReasonInternal     Reason = "Internal error"
	RpcConnectionErr   Reason = "Failed to connect to rpc server"
	RpcInvalidToken    Reason = "Invalid token"
	RpcReturnErr       Reason = "Rpc server return error"
	MQSendErr          Reason = "Message failed to send to RabbitMQ"
	APIBadRequest      Reason = "Api server send malformed message"
)

type Error struct {
	RawErr  interface{}
	Status  Status
	Reason  Reason
	Comment string
	Context string
}

func New(raw interface{}, st Status, r Reason, comment, context string) *Error {
	return &Error{
		RawErr:  raw,
		Status:  st,
		Reason:  r,
		Comment: comment,
		Context: context,
	}
}

func NewInternal(raw interface{}, context string) *Error {
	return &Error{
		RawErr:  raw,
		Status:  StatusInternalErr,
		Reason:  ReasonInternal,
		Context: context,
	}
}


func (e *Error) Error() string {
	return string(e.Status)
}

func (e *Error) AddCtx(ctx string) *Error {
	e.Context = fmt.Sprintf("%s->%s",ctx, e.Context)
	return e
}
